"use strict";
/*
Author: Ryan Hendrickson
Date: February 4th, 2019.
Filename: ES6.js
Purpose: Grab information from our index.html and help us solve the Pythagorean Theorem for our web page user.
*/

let calculate = () => {
	let a = document.getElementById('a').value;
	let b = document.getElementById('b').value;
	let c = document.getElementById('c').value;

	a = Math.pow(a, 2);
	b = Math.pow(b, 2);
	c = Math.pow(c, 2);

	if (a != null && b != null && c == "") {
		document.getElementById('output').textContent = "Side c is: "+(a+b) + " or " + Math.sqrt((a+b));
	} else if (a != null && b == "" && c != null) {
		document.getElementById('output').textContent = "Side b is: "+(c-a) + " or " + Math.sqrt((c-a));
	} else if (a == "" && b != null && c != null) {
		document.getElementById('output').textContent = "Side a is: "+(c-b) + " or " + Math.sqrt((c-b));
	} else {
		document.getElementById('output').textContent = "Please only enter 2 sides...";
	}
}

let resetForm = () => {
	document.getElementById('output').textContent = "";
}
