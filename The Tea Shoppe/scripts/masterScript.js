/*
Author: Ryan Hendrickson
Date: January 7th, 2018.
Filename: masterScript.js
Purpose: Take the name from the user and how many cups of tea they want, calculate a total, and output the total.
*/

// Set constant variables, including our tax rate and TOTAL_RATE to multiply our cupsOfTea by.
var TAX_RATE = .09;
var TOTAL_RATE = (1+TAX_RATE);

function orderSummary()
{
	// Get our variables set up, getting our information via id's
	var name = document.getElementById('name').value;
	var cupsOfTea = document.getElementById('cups').value;
	var biscotti = document.getElementById('biscotti').checked;

	// Send our cupsOfTea to the function that will do the math for us to get the cost for the tea
	var total = priceForTea(cupsOfTea);

	// See if they want biscotti, if they do then add $2 (plus taxes) to their total
	if(biscotti) {
		total = total + (2*TOTAL_RATE);
	}

	// We will use this to change our <h3> "orderSummary" to the correct output
	document.getElementById('orderSummary').innerHTML = "Hello " + name + ", your total is $" + Number(total).toFixed(2);
	// Number(total).tofixed(2) makes number output go 2 decimal places instead of everything
}

// Takes our parameter and multiplies it by our TOTAL_RATE, then returns the cost after tax
function priceForTea(n)
{
	n = n * 1.5;
	return (n*TOTAL_RATE);
}

// When a user clicks "reset" button, it causes the "orderSummary" to become blank again
function resetSummary()
{
	document.getElementById('orderSummary').innerHTML = "";
}
