/*
Author: Ryan Hendrickson
Date: March 18th, 2019.
Filename: dice_roller.js
Purpose: Do the magic behind our dice rolling website. Uses 6 sided die
*/

// This is the function that gets called when the user clicks the button
let dieRoll = () => {
	// Get the value of the amount of dice the user wants to roll and what total they want
	let diceAmnt = document.getElementById('numOfDice').value;
	diceAmnt = parseInt(diceAmnt);
	let totalNum = document.getElementById('roll').value;
	totalNum = parseInt(totalNum);
	// Clear our result field
	document.getElementById('result').textContent = "";
	// Let's see if our user entered an appropriate number of dice | ie more than 0
	if (diceAmnt < 1) {
		document.getElementById('result').textContent = "You can't roll " + diceAmnt + " dice!"
	} else if (totalNum < diceAmnt || (diceAmnt * 6) < totalNum) { // This checks to see if we can even get the number they want with the number of dice.
		document.getElementById('result').textContent = "It's not possible to get " + totalNum + " with " + diceAmnt + " dice...";
	} else {
		// Declare a variable and set it to 0 to keep track of what results pop up in our for loop
		let total = 0;
		let count = 0;
		while(true) {
			for (var i = 1; i <= diceAmnt; i++) {
				// Here we generate a random number, multiply it by 6, then add 1 (so our results are 1-6) then we make the result div on the page the result we get
				let result = Math.floor((Math.random() * 6) + 1);
				total += result; // Slowly add the results together
				// Append all the numbers that get rolled
				document.getElementById('result').textContent += result + " ";
			}
			// Finally append our --> and the total
			document.getElementById('result').textContent += " --> Total: " + total + "\n";
			count++;
			// If our total matches what it should, then we return false which breaks our while loop otherwise we set total to 0
			if(total == totalNum) {
				document.getElementById('result').textContent += "You got your total in " + count + " rolls!";
				return false;
			} else {
				total = 0;
			}
		}
	}
}
